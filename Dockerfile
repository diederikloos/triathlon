FROM python:3.6
WORKDIR /opt/app
RUN pip install pipenv 
ADD Pipfile* /opt/app/
RUN pipenv install --system
ADD . /opt/app
CMD ["/opt/app/entrypoint.sh"]


